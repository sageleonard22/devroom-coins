package me.goswimmy.coins;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        //Setup Data Files
        DataManager.setup(this);
        //Setup Listener(s)
        Bukkit.getPluginManager().registerEvents(new CoinEvents(), this);
        //Setup Command(s)
        getCommand("coins").setExecutor(new CoinCommand());
        //Setup Current Players
        for(Player all : Bukkit.getOnlinePlayers()) {
            SQLManager.initializePlayer(all);
        }
        //Setup MySQL
        SQLManager.initalizePlugin();
        //Setup Placeholder
        if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
            new PlaceholderManager().register();
        }
        //Enabled Message
        System.out.println("Plugin Enabled!");
    }
}
