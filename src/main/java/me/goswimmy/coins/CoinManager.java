package me.goswimmy.coins;

import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CoinManager {

    public static Integer getCoins(Player p) {
        if (SQLManager.connect()) {
            if (SQLManager.isConnected()) {
                Connection connection = SQLManager.getConnection();
                try {
                    String sql = "SELECT * FROM `coins` WHERE uuid = ?";
                    PreparedStatement stmt = connection.prepareStatement(sql);
                    stmt.setString(1, p.getUniqueId().toString());
                    ResultSet res = stmt.executeQuery();
                    if (res.next()) {
                        return res.getInt("coins");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }

    public static void setCoins(Player p, int amount) {
        if (SQLManager.connect()) {
            if (SQLManager.isConnected()) {
                Connection connection = SQLManager.getConnection();
                try {
                    String sql = "UPDATE `coins` SET coins = ? WHERE uuid = ?";
                    PreparedStatement stmt = connection.prepareStatement(sql);
                    stmt.setInt(1, amount);
                    stmt.setString(2, p.getUniqueId().toString());
                    stmt.executeUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void removeCoins(Player p, int amount) {
        if (SQLManager.connect()) {
            if (SQLManager.isConnected()) {
                Connection connection = SQLManager.getConnection();
                try {
                    String sql = "UPDATE `coins` SET coins = coins - ? WHERE uuid = ?";
                    PreparedStatement stmt = connection.prepareStatement(sql);
                    stmt.setInt(1, amount);
                    stmt.setString(2, p.getUniqueId().toString());
                    stmt.executeUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void addCoins(Player p, int amount) {
        if (SQLManager.connect()) {
            if (SQLManager.isConnected()) {
                Connection connection = SQLManager.getConnection();
                try {
                    String sql = "UPDATE `coins` SET coins = coins + ? WHERE uuid = ?";
                    PreparedStatement stmt = connection.prepareStatement(sql);
                    stmt.setInt(1, amount);
                    stmt.setString(2, p.getUniqueId().toString());
                    stmt.executeUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void payCoins(Player p, Player t, int amount) {
        if (SQLManager.connect()) {
            if (SQLManager.isConnected()) {
                Connection connection = SQLManager.getConnection();
                try {
                    String sql = "UPDATE `coins` SET coins = coins + ? WHERE uuid = ?";
                    PreparedStatement stmt = connection.prepareStatement(sql);
                    stmt.setInt(1, amount);
                    stmt.setString(2, t.getUniqueId().toString());
                    stmt.executeUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    String sql = "UPDATE `coins` SET coins = coins - ? WHERE uuid = ?";
                    PreparedStatement stmt = connection.prepareStatement(sql);
                    stmt.setInt(1, amount);
                    stmt.setString(2, p.getUniqueId().toString());
                    stmt.executeUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
