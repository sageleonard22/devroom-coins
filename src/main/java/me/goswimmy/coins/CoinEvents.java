package me.goswimmy.coins;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class CoinEvents implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        SQLManager.initializePlayer(e.getPlayer());
    }

}
