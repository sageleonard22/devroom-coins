package me.goswimmy.coins;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;

public class PlaceholderManager extends PlaceholderExpansion {

    public String getIdentifier() {
        return "coins";
    }

    public String getPlugin() {
        return null;
    }

    public String getAuthor() {
        return "GoSwimmy";
    }

    public String getVersion() {
        return "1.0";
    }

    public String onPlaceholderRequest(Player player, String identifier) {
        if (player == null) {
            return "";
        }
        if (identifier.equalsIgnoreCase("balance")) {
            return String.valueOf(CoinManager.getCoins(player));
        }
        return null;
    }
}
