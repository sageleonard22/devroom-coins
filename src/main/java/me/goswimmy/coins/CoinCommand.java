package me.goswimmy.coins;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class CoinCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player) sender;
        FileConfiguration c = DataManager.config;
        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("info")) {
                p.sendMessage(Util.c(c.getString("messages.info-self").replace("%coins%", String.valueOf(CoinManager.getCoins(p)))));
                return false;
            }
        }
        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("info")) {
                Player t = Bukkit.getPlayer(args[1]);
                if(t == null) {
                    p.sendMessage(Util.c(c.getString("messages.no-target")));
                } else {
                    p.sendMessage(Util.c(c.getString("messages.info").replace("%player%", t.getName()).replace("%coins%", String.valueOf(CoinManager.getCoins(t)))));
                }
                return false;
            }
        }
        if(args.length == 3) {
            if(args[0].equalsIgnoreCase("pay")) {
                Player t = Bukkit.getPlayer(args[1]);
                if(t == null) {
                    p.sendMessage(Util.c(c.getString("messages.no-target")));
                } else {
                    if(p == t) {
                        p.sendMessage(Util.c(c.getString("messages.pay-self")));
                    } else {
                        try{
                            int amount = Integer.parseInt(args[2]);
                            CoinManager.payCoins(p, t, amount);
                            p.sendMessage(Util.c(c.getString("messages.pay-to").replace("%player%", t.getName()).replace("%coins%", String.valueOf(amount))));
                            t.sendMessage(Util.c(c.getString("messages.pay-from").replace("%player%", t.getName()).replace("%coins%", String.valueOf(amount))));
                        } catch(IllegalArgumentException e) {
                            p.sendMessage(Util.c(c.getString("messages.invalid-usage")));
                        }
                    }
                }
                return false;
            }
            if(args[0].equalsIgnoreCase("set")) {
                if(p.hasPermission(c.getString("permissions.admin"))) {
                    Player t = Bukkit.getPlayer(args[1]);
                    if(t == null) {
                        p.sendMessage(Util.c(c.getString("messages.no-target")));
                    } else {
                        try{
                            int amount = Integer.parseInt(args[2]);
                            CoinManager.setCoins(t, amount);
                            p.sendMessage(Util.c(c.getString("messages.admin-set").replace("%player%", t.getName()).replace("%coins%", String.valueOf(amount))));
                        } catch(IllegalArgumentException e) {
                            p.sendMessage(Util.c(c.getString("messages.invalid-usage")));
                        }
                    }
                } else {
                    p.sendMessage(Util.c(c.getString("messages.no-permission")));
                }
                return false;
            }
            if(args[0].equalsIgnoreCase("remove")) {
                if(p.hasPermission(c.getString("permissions.admin"))) {
                    Player t = Bukkit.getPlayer(args[1]);
                    if(t == null) {
                        p.sendMessage(Util.c(c.getString("messages.no-target")));
                    } else {
                        try{
                            int amount = Integer.parseInt(args[2]);
                            CoinManager.removeCoins(t, amount);
                            p.sendMessage(Util.c(c.getString("messages.admin-remove").replace("%player%", t.getName()).replace("%coins%", String.valueOf(amount))));
                        } catch(IllegalArgumentException e) {
                            p.sendMessage(Util.c(c.getString("messages.invalid-usage")));
                        }
                    }
                } else {
                    p.sendMessage(Util.c(c.getString("messages.no-permission")));
                }
                return false;
            }
            if(args[0].equalsIgnoreCase("add")) {
                if(p.hasPermission(c.getString("permissions.admin"))) {
                    Player t = Bukkit.getPlayer(args[1]);
                    if(t == null) {
                        p.sendMessage(Util.c(c.getString("messages.no-target")));
                    } else {
                        try{
                            int amount = Integer.parseInt(args[2]);
                            CoinManager.addCoins(t, amount);
                            p.sendMessage(Util.c(c.getString("messages.admin-add").replace("%player%", t.getName()).replace("%coins%", String.valueOf(amount))));
                        } catch(IllegalArgumentException e) {
                            p.sendMessage(Util.c(c.getString("messages.invalid-usage")));
                        }
                    }
                } else {
                    p.sendMessage(Util.c(c.getString("messages.no-permission")));
                }
                return false;
            }
        }
        for(String msg : c.getStringList("messages.help")) {
            if(msg.startsWith("a:")){
                if(p.hasPermission(c.getString("permissions.admin"))){
                    p.sendMessage(Util.c(msg.substring(2)));
                }
            } else {
                p.sendMessage(Util.c(msg));
            }
        }
        return false;
    }

}

